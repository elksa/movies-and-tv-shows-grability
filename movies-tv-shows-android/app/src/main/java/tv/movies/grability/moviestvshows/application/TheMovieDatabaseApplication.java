package tv.movies.grability.moviestvshows.application;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * @author Alejandro Casanova Mutis, created on 12/01/17.
 */

public class TheMovieDatabaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Init Leak Canary
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
    }
}
