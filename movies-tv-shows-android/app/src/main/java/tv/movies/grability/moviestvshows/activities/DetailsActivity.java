package tv.movies.grability.moviestvshows.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import tv.movies.grability.moviestvshows.R;
import tv.movies.grability.moviestvshows.configuration.AppConstants;
import tv.movies.grability.moviestvshows.model.gson.Movie;
import tv.movies.grability.moviestvshows.model.gson.MovieDetails;
import tv.movies.grability.moviestvshows.presenters.DetailsPresenter;
import tv.movies.grability.moviestvshows.services.TheMovieDataBaseService;

/**
 * @author  Alejandro Casanova Mutis, created on 14/5/17.
 */

public class DetailsActivity extends AppCompatActivity {

    // Constants
    public static final String PARAM_MOVIE_ID = "movie_id";
    public static final String PARAM_BACKDROP_URL = "backdrop_url";
    public static final String PARAM_OVERVIEW = "overview";
    public static final String PARAM_TITLE = "title";
    // Widgets
    protected @Bind(R.id.img_banner)
    ImageView imgBanner;
    protected @Bind(R.id.fab) FloatingActionButton btnFab;
    protected @Bind(R.id.toolbar) Toolbar toolbar;
    protected @Bind(R.id.txt_details_description)
    TextView txtDescription;
    // Fields
    private int idMovie;
    private DetailsPresenter presenter;
    private MovieDetails movieDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        btnFab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(DetailsActivity.this, R.string.btn_show_web, Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        btnFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(movieDetails.getHomepage()));
                    startActivity(browserIntent);
                }
                catch (ActivityNotFoundException e) {
                    Toast.makeText(DetailsActivity.this, R.string.error_no_browser,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().getExtras() != null) {
            this.idMovie = getIntent().getIntExtra(PARAM_MOVIE_ID, 0);

            setTitle(getIntent().getStringExtra(PARAM_TITLE));
            // Load banner
            Picasso.with(this)
                    .load(getIntent().getStringExtra(PARAM_BACKDROP_URL))
                    .placeholder(R.drawable.img_error)
                    .error(R.drawable.img_error)
                    .into(this.imgBanner);

            // Load description
            txtDescription.setText(getIntent().getStringExtra(PARAM_OVERVIEW));

            if (this.idMovie != 0) {
                this.presenter = new DetailsPresenter(this, new TheMovieDataBaseService());
                this.presenter.loadMovieDetails(this.idMovie);
            }

        }
    }

    public void onErrorLoadingData(Throwable e) {
        // TODO Finish
        e.printStackTrace();
        Log.e(MainActivity.class.getName(), e.toString());
    }

    public void onDataLoaded(MovieDetails movieDetails) {

        this.movieDetails = movieDetails;
        // Load title
        //setTitle(movieDetails.getTitle());
        // Load banner
        /*Picasso.with(this)
                .load(movieDetails.getBackdropUrl())
                .placeholder(R.drawable.img_error)
                .error(R.drawable.img_error)
                .into(this.imgBanner);*/

        // Load description
        //txtDescription.setText(this.movieDetails.getOverview());
    }
}
