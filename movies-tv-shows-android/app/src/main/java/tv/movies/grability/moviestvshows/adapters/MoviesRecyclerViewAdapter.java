package tv.movies.grability.moviestvshows.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import tv.movies.grability.moviestvshows.R;
import tv.movies.grability.moviestvshows.configuration.AppConstants;
import tv.movies.grability.moviestvshows.model.gson.Movie;

/**
 * @author  Alejandro Casanova Mutis, created on 14/5/17.
 */

public class MoviesRecyclerViewAdapter extends RecyclerView.Adapter<MoviesRecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

    // Fields
    private List<Movie> items;
    private Context context;
    // Events
    private OnItemClickListener onItemClickListener;

    public MoviesRecyclerViewAdapter(List<Movie> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_movie, null);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Movie item = items.get(position);

        Picasso.with(context)
                .load(item.getPosterUrl())
                .placeholder(R.drawable.img_error)
                .error(R.drawable.img_error)
                .into(holder.imgCover);

        holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClick(final View v) {
        // Give some time to the ripple to finish the effect
        if (onItemClickListener != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onItemClickListener.onItemClick(v, (Movie) v.getTag());
                }
            }, AppConstants.DURATION_RIPPLE_EFFECT);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.img_list_item_movie_cover) ImageView imgCover;

        ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {

        void onItemClick(View view, Movie viewModel);
    }
}
