package tv.movies.grability.moviestvshows.presenters;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tv.movies.grability.moviestvshows.activities.DetailsActivity;
import tv.movies.grability.moviestvshows.activities.MainActivity;
import tv.movies.grability.moviestvshows.model.gson.MovieDetails;
import tv.movies.grability.moviestvshows.model.gson.MovieSearchResult;
import tv.movies.grability.moviestvshows.services.TheMovieDataBaseService;

/**
 * @author  Alejandro Casanova Mutis, created on 14/5/17.
 */

public class DetailsPresenter {

    // Fields
    private DetailsActivity view;
    private TheMovieDataBaseService service;

    public DetailsPresenter(DetailsActivity detailsActivity, TheMovieDataBaseService service) {
        this.view = detailsActivity;
        this.service = service;
    }

    public void loadMovieDetails(int idMovie) {

        this.service.getApi().getMovieDetails(idMovie, new Callback<MovieDetails>() {
            @Override
            public void success(MovieDetails movieDetails, Response response) {
                view.onDataLoaded(movieDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                view.onErrorLoadingData(error);
            }
        });
    }

}
