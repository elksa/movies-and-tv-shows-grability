package tv.movies.grability.moviestvshows.services;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import tv.movies.grability.moviestvshows.configuration.AppConstants;
import tv.movies.grability.moviestvshows.model.gson.MovieDetails;
import tv.movies.grability.moviestvshows.model.gson.MovieSearchResult;

/**
 * @author Alejandro Casanova Mutis, created on 14/5/17.
 */

public class TheMovieDataBaseService {

    // Fields
    private TheMovieDatabaseApi bookApi;

    public TheMovieDataBaseService() {

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader(AppConstants.HEADER_NAME_ACCEPT, AppConstants.CONTENT_TYPE_APP_JSON);
                request.addEncodedQueryParam(AppConstants.PARAMETER_API_KEY, AppConstants.TMDB_API_KEY);
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(AppConstants.URL_TMDB_ENDPOINT)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        this.bookApi = restAdapter.create(TheMovieDatabaseApi.class);
    }

    public TheMovieDatabaseApi getApi() {
        return this.bookApi;
    }

    public interface TheMovieDatabaseApi {

        @GET(AppConstants.OPERATION_MOVIE_CATEGORY_POPULAR)
        void getPopularMovies(@Query(AppConstants.PARAMETER_PAGE) int page, Callback<MovieSearchResult> cb);

        @GET(AppConstants.OPERATION_MOVIE_CATEGORY_TOP_RATED)
        void getTopRatedMovies(@Query(AppConstants.PARAMETER_PAGE) int page, Callback<MovieSearchResult> cb);

        @GET(AppConstants.OPERATION_MOVIE_CATEGORY_UPCOMING)
        void getUpcomingMovies(@Query(AppConstants.PARAMETER_PAGE) int page, Callback<MovieSearchResult> cb);

        @GET(AppConstants.OPERATION_MOVIE_DETAILS + "{movie_id}")
        void getMovieDetails(@Path("movie_id") int idMovie, Callback<MovieDetails> cb);
    }

}
