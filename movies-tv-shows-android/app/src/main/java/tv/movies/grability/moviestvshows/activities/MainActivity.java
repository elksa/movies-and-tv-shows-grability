package tv.movies.grability.moviestvshows.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import tv.movies.grability.moviestvshows.R;
import tv.movies.grability.moviestvshows.adapters.MoviesRecyclerViewAdapter;
import tv.movies.grability.moviestvshows.model.gson.Movie;
import tv.movies.grability.moviestvshows.presenters.MainPresenter;
import tv.movies.grability.moviestvshows.services.TheMovieDataBaseService;

/**
 * @author  Alejandro Casanova Mutis, created on 14/5/17.
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Widgets
    protected @Bind(R.id.toolbar)
    Toolbar toolbar;

    protected @Bind(R.id.drawer_layout)
    DrawerLayout drawer;

    protected @Bind(R.id.nav_view)
    NavigationView navigationView;

    protected @Bind(android.R.id.list)
    RecyclerView recyclerView;

    protected @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    // Fields
    private MoviesRecyclerViewAdapter adapter;
    private List<Movie> items;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        // TODO Finish pagination, currently loading just page # 1
        this.presenter = new MainPresenter(this, new TheMovieDataBaseService());
        this.loadMovies(R.id.nav_popular, 1);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_popular:
                this.loadMovies(R.id.nav_popular, 1);
                break;
            case R.id.nav_top_rated:
                this.loadMovies(R.id.nav_top_rated, 1);
                break;
            case R.id.nav_upcoming:
                this.loadMovies(R.id.nav_upcoming, 1);
                break;
            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadMovies(int category, int page) {

        int titleRes;

        switch (category) {
            case R.id.nav_popular:
                presenter.loadPopularMovies(page);
                titleRes = R.string.menu_popular;
                break;
            case R.id.nav_top_rated:
                presenter.loadTopRatedMovies(page);
                titleRes = R.string.menu_top_rated;
                break;
            case R.id.nav_upcoming:
                titleRes = R.string.menu_upcoming;
                presenter.loadUpcomingMovies(page);
                break;
            default:
                titleRes = R.string.menu_popular;
                presenter.loadPopularMovies(page);
                break;
        }

        getSupportActionBar().setTitle(titleRes);

        this.progressBar.setVisibility(View.VISIBLE);
    }

    public void onErrorLoadingData(Throwable e) {
        // TODO Finish
        e.printStackTrace();
        Log.e(MainActivity.class.getName(), e.toString());
    }

    public void onDataLoaded(List<Movie> data) {

        if (this.items == null) {
            this.items = data;

            LinearLayoutManager layoutManager = new GridLayoutManager(this, 3);
            this.adapter = new MoviesRecyclerViewAdapter(this.items, this);
            this.adapter.setOnItemClickListener(new MoviesRecyclerViewAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, Movie viewModel) {
                    Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                    intent.putExtra(DetailsActivity.PARAM_MOVIE_ID, viewModel.getId());
                    intent.putExtra(DetailsActivity.PARAM_BACKDROP_URL, viewModel.getBackdropUrl());
                    intent.putExtra(DetailsActivity.PARAM_OVERVIEW, viewModel.getOverview());
                    intent.putExtra(DetailsActivity.PARAM_TITLE, viewModel.getTitle());
                    startActivity(intent);
                }
            });
            this.recyclerView.setLayoutManager(layoutManager);
            this.recyclerView.setAdapter(this.adapter);
        }
        else {
            this.items.clear();
            this.items.addAll(data);

            this.adapter.notifyDataSetChanged();
        }

        if (data.size() == 0) {
            Toast.makeText(this, R.string.txt_no_data, Toast.LENGTH_SHORT).show();
        }

        this.progressBar.setVisibility(View.GONE);
    }

}
