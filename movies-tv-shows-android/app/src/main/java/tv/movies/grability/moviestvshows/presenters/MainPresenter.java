package tv.movies.grability.moviestvshows.presenters;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tv.movies.grability.moviestvshows.activities.MainActivity;
import tv.movies.grability.moviestvshows.model.gson.MovieSearchResult;
import tv.movies.grability.moviestvshows.services.TheMovieDataBaseService;

/**
 * @author  Alejandro Casanova Mutis, created on 14/5/17.
 */

public class MainPresenter {

    // Fields
    private MainActivity view;
    private TheMovieDataBaseService service;

    public MainPresenter(MainActivity mainActivity, TheMovieDataBaseService service) {
        this.view = mainActivity;
        this.service = service;
    }

    public void loadPopularMovies(int page) {

        this.service.getApi().getPopularMovies(page, new Callback<MovieSearchResult>() {
            @Override
            public void success(MovieSearchResult movieSearchResult, Response response) {
                view.onDataLoaded(movieSearchResult.getMovies());
            }

            @Override
            public void failure(RetrofitError error) {
                view.onErrorLoadingData(error);
            }
        });
    }

    public void loadTopRatedMovies(int page) {

        this.service.getApi().getTopRatedMovies(page, new Callback<MovieSearchResult>() {
            @Override
            public void success(MovieSearchResult movieSearchResult, Response response) {
                view.onDataLoaded(movieSearchResult.getMovies());
            }

            @Override
            public void failure(RetrofitError error) {
                view.onErrorLoadingData(error);
            }
        });
    }

    public void loadUpcomingMovies(int page) {

        this.service.getApi().getUpcomingMovies(page, new Callback<MovieSearchResult>() {
            @Override
            public void success(MovieSearchResult movieSearchResult, Response response) {
                view.onDataLoaded(movieSearchResult.getMovies());
            }

            @Override
            public void failure(RetrofitError error) {
                view.onErrorLoadingData(error);
            }
        });
    }

}
