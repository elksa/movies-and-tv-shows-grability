package tv.movies.grability.moviestvshows.model;

/**
 * @author  Alejandro Casanova Mutis, created on 14/5/17.
 */

public class TheMovieDatabase {
    private static final TheMovieDatabase ourInstance = new TheMovieDatabase();

    public static TheMovieDatabase getInstance() {
        return ourInstance;
    }

    private TheMovieDatabase() {
    }
}
