package tv.movies.grability.moviestvshows.configuration;

/**
 * @author  Alejandro Casanova Mutis, created on 14/5/17.
 */

public class AppConstants {
	// General
	public static final int SPLASH_TIMEOUT= 2000;
	public static final boolean LOG = true;
	public static final String LANGUAGE = "language";
	public static final int DURATION_RIPPLE_EFFECT = 80;
	// Requests
	public static final String URL_TMDB_ENDPOINT = "http://api.themoviedb.org/3";
	public static final String URL_TMDB_BASE_IMAGE = "https://image.tmdb.org/t/p/w300/";
	public static final String TMDB_API_KEY = "d88eb28576e254ad001febc2ee2ea6d2";
	// Operations
	public static final String OPERATION_MOVIE_CATEGORY_POPULAR = "/movie/popular";
	public static final String OPERATION_MOVIE_CATEGORY_TOP_RATED = "/movie/top_rated";
	public static final String OPERATION_MOVIE_CATEGORY_UPCOMING = "/movie/upcoming";
	public static final String OPERATION_MOVIE_DETAILS = "/movie/";
	// Parameters
	public static final String PARAMETER_API_KEY = "api_key";
	public static final String PARAMETER_PAGE = "page";
	public static final String PARAMETER_LANGUAGE = "language";

	public static final String CONTENT_TYPE_APP_JSON = "application/json";
	public static final String HEADER_NAME_ACCEPT = "Accept";


}
